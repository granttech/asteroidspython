import copy
import random

import pygame


class Particle:

    def __init__(self, position, size, velocity, lifetime, deceleration=0.01, colour=(-1, 0, 0)):
        self.position = copy.deepcopy(position)
        self.size = size
        self.velocity = copy.deepcopy(velocity)
        self.lifetime = lifetime
        self.deceleration = deceleration
        self.expired = False
        if colour[0] == -1:
            # choose random colour
            self.colour = (random.randint(100, 255), random.randint(100, 255), random.randint(100, 255))
        else:
            self.colour = colour

    def move(self):
        self.velocity.move_point(self.position)
        self.velocity.decelerate(self.deceleration)
        # dim colour
        self.colour = (int(self.colour[0] * 0.98), int(self.colour[1] * 0.98), int(self.colour[2] * 0.98))
        self.lifetime -= 1
        if self.lifetime <= 0:
            self.expired = True

    def draw(self, screen):
        if not self.expired:
            pygame.draw.circle(screen, self.colour, self.position.toIntList(), self.size)
