import math
from typing import List

import vector
from GameSettings import GameSettings
from GameStates import GameStates
from PlayerBullet import PlayerBullet
from ShapeNode import ShapeNode
from Point import Point
from Velocity import Velocity


class PlayerShip:
    velocity: Velocity
    position: Point
    nodes: List[ShapeNode]

    def __init__(self):
        self.nodes = [
            ShapeNode(20, 0, (0, 0, 0), True, True),
            ShapeNode(-10, 10, (255, 0, 0), False, True),
            ShapeNode(-5, 5, (255, 0, 0), False, True),
            ShapeNode(-5, -5, (255, 0, 0), False, True),
            ShapeNode(-10, -10, (255, 0, 0), False, True),
            ShapeNode(20, 0, (255, 0, 0), False, True)
        ]
        self.reset()

        # constants
        self.rotation_speed = 0.05
        self.deceleration = 0.02
        self.acceleration = 0.2
        self.max_bullets = 5
        self.scale = 1
        self.diameter = 30

    def rotate_left(self):
        self.rotation -= self.rotation_speed
        self.limit_rotation()

    def rotate_right(self):
        self.rotation += self.rotation_speed
        self.limit_rotation()

    def limit_rotation(self):
        if (self.rotation >= (math.pi * 2)):
            self.rotation -= (math.pi * 2)
        elif (self.rotation < 0):
            self.rotation += (math.pi * 2)

    def thrust(self):
        GameSettings.sound_library.play("thrust")
        thrust_vel = Velocity(self.rotation, self.acceleration)
        self.velocity.add(thrust_vel)

    def move(self):
        self.velocity.move_point(self.position)
        self.position.wrap_around()
        self.velocity.decelerate(self.deceleration)

    def draw(self, screen):
        vector.draw_shape(screen, self)

    def fire(self, player_bullets):
        if (len(player_bullets) < self.max_bullets) and GameSettings.game_state is not GameStates.PLAYER_HIT:
            GameSettings.sound_library.play("fire")
            player_bullets.append(PlayerBullet(self.position, self.rotation))

    def reset_position(self):
        self.position = Point(GameSettings.screenSize["x"] / 2, GameSettings.screenSize["y"] / 2)

    def reset(self):
        self.reset_position()
        self.rotation = math.pi * 3 / 2
        self.velocity = Velocity()
