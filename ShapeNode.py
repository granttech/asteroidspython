class ShapeNode:

    def __init__(self, x, y, colour=(255, 255, 255), move_only=False, outline=False):
        self.x = x
        self.y = y
        self.colour = colour
        self.move_only = move_only
        self.outline = outline
