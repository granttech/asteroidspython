import math


class Velocity:

    def __init__(self, direction=0, speed=0):
        self.direction = direction
        self.speed = speed

    def add(self, velocity2):
        x1 = self.speed * math.cos(self.direction)
        y1 = self.speed * math.sin(self.direction)
        x2 = velocity2.speed * math.cos(velocity2.direction)
        y2 = velocity2.speed * math.sin(velocity2.direction)
        sum_x = x1 + x2
        sum_y = y1 + y2
        sum_speed = math.sqrt((sum_x * sum_x) + (sum_y * sum_y))
        angle = math.atan2(sum_y, sum_x)
        if (angle < 0):
            angle += (math.pi * 2)

        self.direction = angle
        self.speed = sum_speed

    def decelerate(self, decleration):
        self.speed -= decleration
        if self.speed < 0:
            self.speed = 0

    def move_point(self, point):
        x_movement = self.speed * math.cos(self.direction)
        y_movement = self.speed * math.sin(self.direction)
        point.x += x_movement
        point.y += y_movement
        return point
