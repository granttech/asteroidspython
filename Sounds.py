import os
import pygame


class Sounds:
    sounds = {}

    def load_sounds(self):
        folder = os.path.dirname(__file__)
        folder = os.path.join(folder, "sounds")
        sound = pygame.mixer.Sound(os.path.join(folder, "fire.wav"))
        self.sounds["fire"] = sound
        sound = pygame.mixer.Sound(os.path.join(folder, "bangMedium.wav"))
        self.sounds["explosion"] = sound
        sound = pygame.mixer.Sound(os.path.join(folder, "bangLarge.wav"))
        self.sounds["bigExplosion"] = sound
        sound = pygame.mixer.Sound(os.path.join(folder, "thrust.wav"))
        self.sounds["thrust"] = sound

    def play(self, sound_name):
        this_sound = self.sounds.get(sound_name)
        if this_sound is not None:
            this_sound.play()


