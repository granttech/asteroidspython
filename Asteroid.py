import copy
import math
import random
from typing import List

import vector
from GameSettings import GameSettings
from Particle import Particle
from ShapeNode import ShapeNode
from Point import Point
from Velocity import Velocity


class Asteroid:
    min_initial_velocity = 0.5
    max_initial_velocity = 2
    max_initial_rotation = 0.05
    initial_radius = 40
    radius_variation_positive = 10
    radius_variation_negative = -25
    shape_num_points = 9

    velocity: Velocity
    position: Point
    nodes: List[ShapeNode]

    def __init__(self):

        self.create_shape()
        # start position so that half start at sides, half at top/bottom
        if (random.randint(0, 1) == 0):
            self.position = Point(random.randint(0, GameSettings.screenSize["x"]), 0)
        else:
            self.position = Point(0, random.randint(0, GameSettings.screenSize["y"]))

        self.rotation = 0.0
        velocity_level_up = ((GameSettings.level - 1) * 0.5) + 1
        self.velocity = Velocity(random.uniform(0, (math.pi * 2)),
                                 random.uniform(self.min_initial_velocity * velocity_level_up,
                                                self.max_initial_velocity * velocity_level_up))
        self.scale = 1
        self.hit = False
        self.diameter = self.initial_radius + self.radius_variation_positive

        # constants
        self.rotation_speed = random.uniform(-self.max_initial_rotation, self.max_initial_rotation)

    def rotate(self):
        self.rotation += self.rotation_speed
        self.limit_rotation()

    def limit_rotation(self):
        if (self.rotation >= (math.pi * 2)):
            self.rotation -= (math.pi * 2)
        elif (self.rotation < 0):
            self.rotation += (math.pi * 2)

    def move(self):
        self.velocity.move_point(self.position)
        self.position.wrap_around()
        self.rotate()

    def draw(self, screen):
        vector.draw_shape(screen, self)

    def check_hit(self, player_bullets):
        for bullet in player_bullets:
            if (not bullet.expired):
                if vector.point_within_range(bullet.position, self.position,
                                             (self.initial_radius + self.radius_variation_positive) / self.scale):
                    if vector.point_in_shape(bullet.position, self):
                        self.hit = True
                        bullet.expired = True

    def explode(self, asteroids, particles):
        # size 1 = big, 2 = med, 4 = small
        GameSettings.player_score += 50 * self.scale
        # generate particles for explosion effect
        for count in range(0, random.randint(20, 30)):
            # generate random velocity
            velocity = Velocity(random.uniform(0, math.pi * 2),
                                random.uniform(self.min_initial_velocity, self.max_initial_velocity * 2))
            # add this to asteroid velocity so particles appear to be blown off asteroid
            velocity.add(self.velocity)
            size = random.randint(1, 5)
            particles.append(Particle(self.position, size, velocity, random.randint(30, 90), ))

        if (self.scale < 4):
            self.hit = False
            self.scale *= 2
            top_half = copy.deepcopy(self)
            top_half.create_shape()
            bottom_half = copy.deepcopy(self)
            bottom_half.create_shape()
            velocity_level_up = ((GameSettings.level - 1) * 0.5) + 1
            angle_change = random.uniform(0, math.pi)
            top_half.velocity.direction -= angle_change
            speed_change = random.uniform(-Asteroid.max_initial_velocity * velocity_level_up,
                                          Asteroid.max_initial_velocity * velocity_level_up)
            top_half.velocity.speed += speed_change
            angle_change = random.uniform(0, math.pi)
            bottom_half.velocity.direction += angle_change
            speed_change = random.uniform(-Asteroid.max_initial_velocity * velocity_level_up,
                                          Asteroid.max_initial_velocity * velocity_level_up)
            bottom_half.velocity.speed += speed_change
            asteroids.append(top_half)
            asteroids.append(bottom_half)
            asteroids.remove(self)
        else:
            asteroids.remove(self)

    def create_shape(self):
        # setup shape
        # first point at angle = 0
        self.nodes = [
            ShapeNode(Asteroid.initial_radius, 0, (0, 0, 0), True, True)
        ]
        # more points
        for point in range(1, self.shape_num_points - 1):
            angle = (math.pi * 2) * point / self.shape_num_points
            radius = Asteroid.initial_radius + random.randint(Asteroid.radius_variation_negative,
                                                              Asteroid.radius_variation_positive)
            self.nodes.append(
                ShapeNode(radius * math.cos(angle), radius * math.sin(angle), (255, 255, 255), False, True))

        # final point same as first
        self.nodes.append(ShapeNode(Asteroid.initial_radius, 0, (255, 255, 255), False, True))
