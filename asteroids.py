import math
import os

import pygame

from Asteroid import Asteroid
from GameSettings import GameSettings
import vector
import resize
from GameStates import GameStates
from Particle import Particle
from PlayerShip import PlayerShip
import random
import time

from Velocity import Velocity


def check_keyboard(player_ship, player_bullets):
    ## check keyboard
    if not GameSettings.game_state is GameStates.START and not GameSettings.game_state is GameStates.GAME_OVER:
        keys = pygame.key.get_pressed()
        if keys[pygame.K_z]:
            player_ship.rotate_left()
        if keys[pygame.K_x]:
            player_ship.rotate_right()
        if keys[pygame.K_RSHIFT]:
            player_ship.thrust()


def reset_game():
    GameSettings.player_score = 0
    GameSettings.player_lives = 3
    GameSettings.level = 1

    GameSettings.game_state = GameStates.PLAYING

    GameSettings.last_frame_time = 0
    GameSettings.current_time = 0


def handle_events(screen, player_ship, player_bullets, asteroids):
    # event handling, gets all event from the event queue
    for event in pygame.event.get():
        # only do something if the event is of type QUIT
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                if (GameSettings.game_state is GameStates.START):
                    GameSettings.game_state = GameStates.PLAYING
                    reset_game()
                    player_ship.reset()
                    generate_asteroids(asteroids)
                else:
                    player_ship.fire(player_bullets)

        elif event.type == pygame.QUIT:
            # change the value to False, to exit the main loop
            GameSettings.running = False
        elif event.type == pygame.VIDEORESIZE:
            if event.w < 750:
                event.w = 750
            if event.h < 750:
                event.h = 750
            resize.resize_screen(screen, event.w, event.h, GameSettings)


def generate_asteroids(asteroids):
    # clear out existing asteroids
    asteroids.clear()
    for count in range(0, 10):
        asteroids.append(Asteroid())


def render_game_status(screen):

    textSurface = GameSettings.font_library.use('Arial').render(
        'Score : ' + str(GameSettings.player_score) + '  Level : ' + str(GameSettings.level),
        False, (255, 255, 255))
    screen.blit(textSurface, (0, 0))
    textSurface = GameSettings.font_library.use('Arial').render('Lives : ' + str(GameSettings.player_lives),
                                                                False, (255, 255, 255))
    screen.blit(textSurface, (GameSettings.screenSize["x"] - 100, 0))
    GameSettings.current_time = time.time()
    frame_time = GameSettings.current_time - GameSettings.last_frame_time
    try:
        frame_rate = 1 / frame_time
    except:
        frame_rate = 60
    GameSettings.last_frame_time = GameSettings.current_time
    textSurface = GameSettings.font_library.use('Arial').render('Frame Rate : ' + '{0:0.1f}'.format(frame_rate),
                                                                True, (255, 255, 255))
    screen.blit(textSurface, (0, 16))


def render_start_screen(screen):
    folder = os.path.dirname(__file__)
    folder = os.path.join(folder, "images")
    image = pygame.image.load(os.path.join(folder, "asteroids.png"))
    width, height = image.get_size()
    scale = GameSettings.screenSize["x"] / width
    top = int((GameSettings.screenSize["y"] / 2) - ((height * scale) / 2))
    screen.blit(pygame.transform.scale(image, (int(width * scale), int(height * scale))), (0, top))


def render_game_over_screen(screen):
    folder = os.path.dirname(__file__)
    folder = os.path.join(folder, "images")
    image = pygame.image.load(os.path.join(folder, "game_over.png"))
    width, height = image.get_size()
    scale = (GameSettings.screenSize["x"] * 0.5) / width
    top = int((GameSettings.screenSize["y"] / 2) - ((height * scale) / 2))
    left = int((GameSettings.screenSize["x"] / 2) - ((width * scale) / 2))
    screen.blit(pygame.transform.scale(image, (int(width * scale), int(height * scale))), (left, top))


def main():
    # seed the random numbers
    random.seed()

    # game data
    player_ship = PlayerShip()
    player_bullets = []

    asteroids = []
    generate_asteroids(asteroids)

    particles = []

    # initialize the pygame module
    pygame.init()
    pygame.event.set_allowed([pygame.QUIT, pygame.KEYDOWN, pygame.RESIZABLE])
    pygame.display.set_caption("Asteroids")

    # preload sounds
    GameSettings.sound_library.load_sounds()

    # setup fonts
    pygame.font.init()
    GameSettings.font_library.load_fonts()

    # create a surface on screen
    screen = pygame.display.set_mode((GameSettings.screenSize["x"], GameSettings.screenSize["y"]), pygame.DOUBLEBUF)
    screen.set_alpha(None)

    GameSettings.running = True

    GameSettings.last_frame_time = time.time()

    # main loop
    while GameSettings.running:
        # set max frame rate
        pygame.time.Clock().tick(60)

        # clear screen
        screen.fill((0, 0, 0))

        render_game_status(screen)

        # check if all asteroids destroyed
        if (len(asteroids) == 0):
            # pause before respawn
            if GameSettings.countdown_timer == 0:
                # start timer
                GameSettings.countdown_timer = 60
            else:
                GameSettings.countdown_timer -= 1
                # wait for timer to reach zero
                if GameSettings.countdown_timer == 0:
                    # start next level
                    GameSettings.level += 1
                    generate_asteroids(asteroids)

        # handle bullets
        for bullet in player_bullets:
            bullet.move()
            bullet.draw(screen)
            if bullet.expired:
                player_bullets.remove(bullet)

        # handle asteroids
        for index, asteroid in enumerate(asteroids):
            # only check collision if state is PLAYING
            if GameSettings.game_state is GameStates.PLAYING:
                if vector.shapes_colliding(asteroid, player_ship):
                    GameSettings.game_state = GameStates.PLAYER_HIT
            asteroid.move()
            asteroid.draw(screen)
            asteroid.check_hit(player_bullets)
            if asteroid.hit:
                GameSettings.sound_library.play("explosion")
                asteroid.explode(asteroids, particles)

        # handle particles
        for particle in particles:
            particle.move()
            particle.draw(screen)
            if particle.expired:
                particles.remove(particle)

        # handle game states
        if GameSettings.game_state is GameStates.START:
            render_start_screen(screen)
        elif GameSettings.game_state is GameStates.GAME_OVER:
            render_game_over_screen(screen)
            # set timer for 3 seconds
            if GameSettings.countdown_timer == 0:
                GameSettings.countdown_timer = 180
            else:
                GameSettings.countdown_timer -= 1
                # wait for timer
                if GameSettings.countdown_timer == 0:
                    GameSettings.game_state = GameStates.START
                    generate_asteroids(asteroids)
        elif GameSettings.game_state is GameStates.PLAYING:
            # handle player ship
            player_ship.move()
            player_ship.draw(screen)
        elif GameSettings.game_state is GameStates.PLAYER_HIT:
            # start explosion
            GameSettings.player_lives -= 1
            player_ship.exploding = True
            GameSettings.sound_library.play("bigExplosion")
            for count in range(0, random.randint(50, 70)):
                # generate random velocity
                velocity = Velocity(random.uniform(0, math.pi * 2),
                                    random.uniform(1, 5))
                # add this to asteroid velocity so particles appear to be explode from ship
                velocity.add(player_ship.velocity)
                size = random.randint(1, 5)
                particles.append(Particle(player_ship.position, size, velocity, random.randint(60, 90), ))
            GameSettings.game_state = GameStates.PLAYER_EXPLODING
        elif GameSettings.game_state is GameStates.PLAYER_EXPLODING:
            # wait for all particles to finish
            if (len(particles) <= 0):
                player_ship.regenerate = True
                player_ship.reset_position()
                # check if game over
                if GameSettings.player_lives <= 0:
                    GameSettings.game_state = GameStates.GAME_OVER
                else:
                    # set state for next player ship
                    GameSettings.game_state = GameStates.PLAYER_REGEN
        elif GameSettings.game_state is GameStates.PLAYER_REGEN:
            # wait until no asteroids close to respawn area
            asteroid_too_close = False
            for asteroid in asteroids:
                if vector.point_within_range(player_ship.position, asteroid.position, 100):
                    asteroid_too_close = True
            if not asteroid_too_close:
                # respawn player ship
                player_ship.reset()
                GameSettings.game_state = GameStates.PLAYING

        check_keyboard(player_ship, player_bullets)
        handle_events(screen, player_ship, player_bullets, asteroids)

        pygame.display.flip()


# run the main function only if this module is executed as the main script
# (if you import this as a module then nothing is executed)
if __name__ == "__main__":
    # call the main function
    main()
