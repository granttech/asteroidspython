import pygame


def resize_screen(screen, x, y, game_settings):
    old_screen_saved = screen
    game_settings.screenSize["x"] = x
    game_settings.screenSize["y"] = y
    screen = pygame.display.set_mode((x, y),
                                     pygame.RESIZABLE)
    # On the next line, if only part of the window
    # needs to be copied, there's some other options.
    screen.blit(old_screen_saved, (0, 0))
    del old_screen_saved
