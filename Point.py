from GameSettings import GameSettings


class Point:
    x: float
    y: float

    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def wrap_around(self):
        if (self.x > GameSettings.screenSize["x"]):
            self.x = 0
        elif (self.x < 0):
            self.x = GameSettings.screenSize["x"]

        if (self.y > GameSettings.screenSize["y"]):
            self.y = 0
        elif (self.y < 0):
            self.y = GameSettings.screenSize["y"]

    def toList(self):
        return [self.x, self.y]

    def toIntList(self):
        return [int(self.x), int(self.y)]
