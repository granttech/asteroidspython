from Fonts import Fonts
from GameStates import GameStates
from Sounds import Sounds


class GameSettings:
    # class level attributes accessible to all files

    screenSize = {
        "x": 600,
        "y": 600
    }

    player_score = 0
    player_lives = 3
    level = 1

    game_state = GameStates.START

    running = True
    last_frame_time = 0
    current_time = 0

    countdown_timer = 0

    sound_library = Sounds()
    font_library = Fonts()
