import pygame


class Fonts:

    def __init__(self):
        self.fonts = {}

    def store_font(self, font, font_name):
        self.fonts[font_name] = font

    def load_fonts(self):
        fontArial = pygame.font.SysFont('Arial', 16)
        self.fonts['Arial'] = fontArial

    def use(self, font_name):
        font = self.fonts.get(font_name)
        if font is not None:
            return font


