import copy

import pygame
import math

from Line import Line
from Point import Point


def draw_shape(screen, shape):
    for node in shape.nodes:
        rotated_point = Point(
            ((node.x * math.cos(shape.rotation)) - (node.y * math.sin(shape.rotation))) / shape.scale,
            ((node.y * math.cos(shape.rotation)) + (node.x * math.sin(shape.rotation))) / shape.scale,
        )
        if node.move_only:
            last_point = copy.deepcopy(rotated_point)

        else:
            pygame.draw.line(screen, node.colour,
                             (last_point.x + shape.position.x, last_point.y + shape.position.y),
                             (rotated_point.x + shape.position.x, rotated_point.y + shape.position.y),
                             1)
            last_point = copy.deepcopy(rotated_point)


def point_within_range(point1, point2, min_distance):
    # distance = math.sqrt(((point2.x - point1.x) ** 2) + ((point2.y - point1.y) ** 2))
    distance2 = ((point2.x - point1.x) ** 2) + ((point2.y - point1.y) ** 2)
    if (distance2 <= (min_distance ** 2)):
        return True
    else:
        return False


def point_in_shape(point, shape):
    points_to_right = 0
    points_to_left = 0
    last_point = 0
    for node in shape.nodes:
        rotated_point = Point(
            ((node.x * math.cos(shape.rotation)) - (node.y * math.sin(shape.rotation))) / shape.scale,
            ((node.y * math.cos(shape.rotation)) + (node.x * math.sin(shape.rotation))) / shape.scale,
        )
        if node.move_only:
            last_point = copy.deepcopy(rotated_point)
        else:
            line = Line(Point(last_point.x + shape.position.x, last_point.y + shape.position.y),
                        Point(rotated_point.x + shape.position.x, rotated_point.y + shape.position.y)
                        )
            intercept = line.check_intercept(point)
            if (intercept == 1):
                points_to_left += 1
            elif (intercept == 2):
                points_to_right += 1
            last_point = copy.deepcopy(rotated_point)

    if (points_to_right % 2) == 1:
        return True
    else:
        return False


def shapes_colliding(shape1, shape2):
    if point_within_range(shape1.position, shape2.position,
                          (shape1.diameter / shape1.scale) + (shape2.diameter / shape2.scale)):
        colliding = False
        for node in shape1.nodes:
            rotated_point = Point(
                ((node.x * math.cos(shape1.rotation)) - (node.y * math.sin(shape1.rotation))) / shape1.scale,
                ((node.y * math.cos(shape1.rotation)) + (node.x * math.sin(shape1.rotation))) / shape1.scale,
            )
            rotated_point.x += shape1.position.x
            rotated_point.y += shape1.position.y
            if point_in_shape(rotated_point, shape2):
                return True
        for node in shape2.nodes:
            rotated_point = Point(
                ((node.x * math.cos(shape2.rotation)) - (node.y * math.sin(shape2.rotation))) / shape2.scale,
                ((node.y * math.cos(shape2.rotation)) + (node.x * math.sin(shape2.rotation))) / shape2.scale,
            )
            rotated_point.x += shape2.position.x
            rotated_point.y += shape2.position.y
            if point_in_shape(rotated_point, shape1):
                return True
        return False
    else:
        return False
