from enum import Enum, auto


# enumeration to store game state values
class GameStates(Enum):
    START = auto()
    PLAYING = auto()
    PLAYER_HIT = auto()
    PLAYER_EXPLODING = auto()
    PLAYER_REGEN = auto()
    GAME_OVER = auto()
