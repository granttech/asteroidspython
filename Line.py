import copy
from Point import Point


class Line:
    point2: Point
    point1: Point

    def __init__(self, point1, point2):
        self.point1 = copy.deepcopy(point1)
        self.point2 = copy.deepcopy(point2)

    def check_intercept(self, point):
        # check if line intercepts x axis of point
        # return 0 for no intercept
        # 1 = to left, 2 = to right

        intercept = 0  # default to no intercept
        if ((self.point1.y > point.y) and (self.point2.y <= point.y)) or (
                (self.point2.y > point.y) and (self.point1.y <= point.y)):
            if self.point2.x == self.point1.x:
                if self.point2.x <= point.x:
                    intercept = 1  # left
                else:
                    intercept = 2  # right
            elif self.point2.y == self.point1.y:
                intercept = 0  # no intercept
            else:
                if (self.point1.x <= point.x) and (self.point2.x <= point.x):
                    # line is fully to the left
                    intercept = 1
                elif (self.point1.x >= point.x) and (self.point2.x >= point.x):
                    # line fully to the right
                    intercept = 2
                else:
                    # line spans test point x position
                    gradient = (self.point2.y - self.point1.y) / (self.point2.x - self.point1.x)
                    intercept_x = ((point.y - self.point1.y) / gradient) + self.point1.x
                    if intercept_x <= point.x:
                        intercept = 1  # left
                    else:
                        intercept = 2  # right
        else:
            intercept = 0  # no intercept

        return intercept
