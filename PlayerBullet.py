import copy

import pygame

from Velocity import Velocity


class PlayerBullet:
    speed = 8
    lifetime = 80
    size = 2

    def __init__(self, position, direction):
        self.position = copy.deepcopy(position)
        self.velocity = Velocity(direction, PlayerBullet.speed)
        self.life_counter = PlayerBullet.lifetime
        self.expired = False
        self.scale = 1

    def draw(self, screen):
        if not self.expired:
            pygame.draw.circle(screen, (0, 255, 0), self.position.toIntList(), PlayerBullet.size)

    def move(self):
        self.velocity.move_point(self.position)
        self.position.wrap_around()
        self.life_counter -= 1
        if self.life_counter < 0:
            self.expired = True
